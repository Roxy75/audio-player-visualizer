"use client";

import { useRef, useState, useEffect } from "react";
import styled from "styled-components";
import App from "./components/App";

const AppContainer = styled.section`
	position: relative;
	text-align: center;
	overflow: hidden; /* Pour cacher le débordement de l'élément ::before */
`;

const BlurOverlay = styled.div`
	content: "";
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background-image: url("assets/img/background.png");
	background-position: center;
	background-size: cover;
	filter: blur(5px);
	z-index: -1;
	/* -webkit-filter: blur(5px);
	-moz-filter: blur(5px);
	-o-filter: blur(5px);
	-ms-filter: blur(5px); */
`;

const Header = styled.header`
	display: flex;
	justify-content: center;
	align-items: center;
	padding: 10px;
	background-color: #a76868;
`;

const Logo = styled.img`
	width: 7%;
	display: block;
	justify-content: center;
	padding-right: 13px;
	align-items: center;
`;

const MainContainer = styled.main`
	width: 100%;
	max-width: 765px;
	margin: 0 auto;
	padding: 25px;
`;

const SongTitle = styled.h2`
	color: #504f4f;
	font-size: 30px;
	font-weight: 700;
	text-transform: uppercase;
`;

const SongArtist = styled.span`
	font-weight: 500;
	font-style: italic;
`;

const ControlsContainer = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	padding: 30px 15px 90px 15px;
`;

const StyledButton = styled.button`
	appearance: none;
	background: none;
	border: none;
	outline: none;
	cursor: pointer;
	&:hover {
		opacity: 0.5;
	}
`;

const PlayButton = styled(StyledButton)`
	font-size: 20px;
	font-weight: 700;
	padding: 15px 25px;
	margin: 0px 15px;
	border-radius: 8px;
	color: #fae9e9;
	background-color: #7c0909;
`;

const NextPrevButton = styled(StyledButton)`
	font-size: 15px;
	font-weight: 700;
	padding: 10px 20px;
	margin: 0px 15px;
	border-radius: 6px;
	color: #fae9e9;
	background-color: #b10404;
`;

const ArtistImage = styled.img`
	width: 40%;
	margin: 0 auto;
	display: block;
	padding-bottom: 20px;
`;

const PlaylistContainer = styled.article`
	font-size: 20px;
	font-weight: 400px;
	margin: 50px 0;
	text-align: center;
`;

const PlaylistSong = styled.button`
	display: block;
	width: 100%;
	padding: 15px;
	font-size: 20px;
	font-weight: 700;
	cursor: pointer;
	&:hover {
		color: #b10404;
	}
	&.playing {
		color: #fae9e9;
		background-image: linear-gradient(to right, #7c0909, #b10404);
	}
`;

const Home = () => {
	const [current, setCurrent] = useState({});
	const [isPlaying, setIsPlaying] = useState(false);
	const [index, setIndex] = useState(0);
	const [player, setPlayer] = useState(null);
	const [songs, setSongs] = useState([
		{
			id: 1,
			title: "We are stars",
			artist: "Callum Beattie",
			image: "assets/artists/album-callum-beattie.jpg",
			src: "assets/music/Callum-Beattie-We-are-stars.mp3",
		},
		{
			id: 2,
			title: "Woman",
			artist: "John Lennon",
			image: "assets/artists/john-lennon-woman.jpg",
			src: "/assets/music/John-Lennon-Woman.mp3",
		},
		{
			id: 3,
			title: "In the afternoon",
			artist: "Josef Salvat",
			image: "assets/artists/Modern-Anxiety.jpg",
			src: "/assets/music/Josef-Salvat-In-the-afternoon.mp3",
		},
		{
			id: 4,
			title: "The only reason",
			artist: "JP Cooper",
			image: "assets/artists/Raised-Under-Grey-Skies.jpg",
			src: "/assets/music/JP-Cooper-The-only-reason.mp3",
		},
		{
			id: 5,
			title: "Caution",
			artist: "The Killers",
			image: "assets/artists/the-killers-caution.jpg",
			src: "/assets/music/The-Killers-Caution.mp3",
		},
	]);
	const audioRef = useRef({
		current: {
			play: () => {},
			pause: () => {},
			src: songs[0].src || "", // Store the current song's URL,
		},
	});
	// const audioRef = useRef();
	//console.log(audioRef);
	// console.log(audioRef.current.current.src);
	useEffect(() => {
		// // Check if window is defined (client side)
		// if (typeof window !== "undefined") {
		// 	// Create the Audio Object only on the client side
		// 	const newPlayer = new Audio();
		// 	audioRef.current = newPlayer;
		// 	setPlayer(newPlayer);
		// }

		// Check if window is defined (client side)
		if (typeof window !== "undefined") {
			// Create the Audio Object only on the client side
			const newPlayer = new Audio();
			audioRef.current = { current: newPlayer }; // Update audioRef with the new player
			setPlayer(newPlayer);
		}
	}, [songs]);
	//console.log(audioRef);

	useEffect(() => {
		setCurrent(songs[index]);
		if (player) {
			player.src = songs[index].src;
			audioRef.current.src = songs[index].src;
		}
	}, [index, songs, player]);

	const play = (song) => {
		// console.log("Play function called");
		// console.log("Player:", player);
		// console.log("Song source:", song.src);

		if (typeof song.src !== "undefined") {
			setCurrent(song);
			player.src = song.src;
			audioRef.current.current.src = song.src;
		}
		// console.log("audioRef after update:", audioRef.current.current.src);

		player.play();
		player.addEventListener("ended", () => {
			setIndex((prevIndex) => (prevIndex + 1) % songs.length);
		});
		setIsPlaying(true);
		player.autoplay = true;
	};

	const pause = () => {
		player.pause();
		setIsPlaying(false);
		player.currentTime = 0;
	};

	const next = () => {
		setIndex((prevIndex) => (prevIndex + 1) % songs.length);
	};

	const prev = () => {
		setIndex((prevIndex) => (prevIndex - 1 + songs.length) % songs.length);
	};

	return (
		<AppContainer>
			<BlurOverlay />
			<Header>
				<Logo
					src="assets/img/headphones-logo.svg"
					alt="Chill and Listen headphones"
				/>
				<h1>Chill and Listen</h1>
			</Header>
			<MainContainer>
				{/* ... */}
				<SongTitle>
					<SongArtist>{current.artist}</SongArtist> - {current.title}
				</SongTitle>

				<ControlsContainer>
					<NextPrevButton onClick={prev}>Prev</NextPrevButton>
					{!isPlaying ? (
						<PlayButton onClick={play}>Play</PlayButton>
					) : (
						<PlayButton onClick={pause}>Pause</PlayButton>
					)}
					<NextPrevButton onClick={next}>Next</NextPrevButton>
				</ControlsContainer>

				<ArtistImage src={current.image} alt="Artist playing" />

				<App
					audioRef={audioRef}
					songs={songs[index]}
					isPlaying={isPlaying}
					play={play}
					pause={pause}
				/>

				{/* ... */}
				<PlaylistContainer>
					<h3>Playlist</h3>
					{songs.map((song) => (
						<PlaylistSong
							key={song.id}
							onClick={() => play(song)}
							className={song.src === current.src ? "song playing" : "song"}
						>
							{song.artist} - {song.title}
						</PlaylistSong>
					))}
				</PlaylistContainer>
			</MainContainer>
		</AppContainer>
	);
};

export default Home;
