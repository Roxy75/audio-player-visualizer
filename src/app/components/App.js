"use client";

import React, { useRef, useState, useEffect, Suspense } from "react";
import { Canvas } from "@react-three/fiber";
import Visualizer from "./Visualizer";
import { PositionalAudio } from "@react-three/drei";
import { EffectComposer, Bloom, SMAA } from "@react-three/postprocessing";
import styles from "../page.module.css";

const App = ({ audioRef, songs, isPlaying, play, pause }) => {
	const sound = useRef();
	// const [play, setPlay] = useState(true);
	const [currentSong, setCurrentSong] = useState(songs);

	// Adding spheres in a circle.
	const createSpheres = () => {
		const number = 20,
			increase = (Math.PI * 2) / number;
		let angle = Math.PI / 2;
		let spheres = [];

		for (let i = 0; i < number; i++) {
			let x = 9 * Math.cos(angle);
			let y = 9 * Math.sin(angle);
			let key = `sphere_${i}`;
			let idx = i < number / 2 ? i : number - i;
			spheres.push(
				<Visualizer
					key={key}
					position={[x, y, -10]}
					radius={0.25}
					angle={angle}
					sound={sound}
					index={idx}
				/>,
			);
			angle += increase;
		}
		return spheres;
	};

	const playMusic = () => {
		if (sound && sound.current) {
			if (isPlaying) {
				sound.current.pause;
			} else {
				sound.current.play;
			}
		}
	};
	playMusic();

	return (
		<div className={styles.App}>
			<Canvas className={styles.canvas}>
				<ambientLight intensity={0.2} />
				<directionalLight position={[0, 0, 5]} />
				<Suspense fallback={null}>
					{/* Pass down audioRef to PositionalAudio */}
					<PositionalAudio
						url={currentSong.src}
						distance={10}
						loop
						ref={audioRef}
					/>
					{createSpheres()}
					{console.log(`song played: ${currentSong.src}`)}
					<EffectComposer multisampling={0}>
						<Bloom
							intensity={0.5}
							luminanceThreshold={0}
							luminanceSmoothing={0.8}
						/>
						<SMAA />
					</EffectComposer>
				</Suspense>
			</Canvas>
		</div>
	);
};

export default App;
