// /** @type {import('next').NextConfig} */
// const nextConfig = {}

// module.exports = nextConfig

import withImages from "next-images";

// MODERN VERSION

export default withImages({
	webpack(config, { isServer }) {
		config.module.rules.push({
			test: /\.(mp3)$/i,
			type: "asset/resource",
			generator: {
				filename: "static/assets/music/[name][ext]",
			},
		});

		return config;
	},
});

// export default withImages({
// 	webpack(config, { isServer }) {
// 		config.module.rules.push({
// 			test: /\.(mp3)$/i,
// 			use: [
// 				{
// 					loader: "file-loader",
// 					options: {
// 						name: "[name].[ext]",
// 						publicPath: "/_next/static/assets/music/", // Adjust the public path as needed
// 						outputPath: "static/assets/music/", // Adjust the output path as needed
// 						esModule: false,
// 					},
// 				},
// 			],
// 		});

// 		return config;
// 	},
// });
